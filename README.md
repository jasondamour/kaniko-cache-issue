# Kaniko Cache Issue

1. Install Lima (I used colima)
2. Run the below command while on `main` branch, observe cache misses.
3. Checkout the WORKING branch and run below command again, observe cache works when using sha256 of the tag

```
docker run \
    -v "$(pwd)"/:/workspace/ \
    -v ~/.config/gcloud/:/root/.config/gcloud/ \
    gcr.io/kaniko-project/executor:v1.9.1-debug \
        --cache=true \
        --cache-repo "us-docker.pkg.dev/jasondamour/public/kaniko-cache-issue/cache" \
        --destination image \
        --tarPath=stack.tar \
        --no-push \
        --no-push-cache
```